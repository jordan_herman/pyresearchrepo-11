
#
import pandas as pd
import pyresearch
import requests
import json
def getparentsbychildlist(mydomains, debug=False):
    # PUBLC

    myipy = get_ipython()
    myipy.run_line_magic(u'hostaxis', 'set hostaxis_display_json False')
    myipy.run_line_magic(u'hostaxis', 'set hostaxis_display_df False')
    frames = []
    for x in mydomains:
        tmp = None
        if debug:
            print(x)
        myipy.run_line_magic(u'hostaxis', "hosts parents '%s' --causes script.src -c -1" % x )
        frames.append(myipy.user_ns['prev_hostaxis_df'])
    output_df = pd.concat(frames)
    myipy.run_line_magic(u'hostaxis', 'set hostaxis_display_df True')
    output_df = pyresearch.util.fixBeakerBool(output_df)
    output_df = output_df.reindex(axis=0)
    return output_df


def hostaxis_iterable(in_list, query, opt):

    myipy = get_ipython()
    myipy.run_line_magic(u'hostaxis', 'set hostaxis_display_json False')
    myipy.run_line_magic(u'hostaxis', 'set hostaxis_display_df False')
    frames = []
    for x in in_list:
        tmp = None
        myipy.run_line_magic(u'hostaxis', query + x + opt)
        frames.append(myipy.user_ns['prev_hostaxis_df'])
    output_df = pd.concat(frames)
    myipy.run_line_magic(u'hostaxis', 'set hostaxis_display_df True')
    output_df = pyresearch.util.fixBeakerBool(output_df)
    output_df = output_df.reindex(axis=0)
    return output_df    