from collections import OrderedDict
from pyresearch.hostaxis.sharedfunctions import *
import pandas as pd
import pyresearch



docs_dict = OrderedDict()

#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }

docs_dict['getparentsbychildlist'] = {'fargs':'mydomains: a list of domains to check for parents\ndebug: show the domain as its looked up',
               'fret': 'output_df: A dataframe that has the results from hostaxis including all parents of the childdomains in the list',
               'shortdesc': 'Check a list of domains for all parent domains in hostaxis',
               'desc': 'getparentsbychildlist(mydomains)\nUsing Hostaxis hosts parents --causes script.src, return the list of all parent URLs where there is a child relationship to one of the domains in the provided host list'
               }
docs_dict['hostaxis_iterable'] = {'fargs':'in_list: a list of some items to run against a hostaxis query\nquery: the hostaxis query you wish to run in string format (do not include the leading hostaxis term with this query\nopt: any options you wish to include with the query in string format',
               'fret': 'output_df: A dataframe that has the results from hostaxis for each item queried',
               'shortdesc': 'Iterate through a list, running hostaxis queries for each item, returns a dataframe with all results for each item queried',
               'desc': 'Iterate through a list, running hostaxis queries for each item, returns a dataframe with all results for each item queried\nUsage example: hosts = ["sequracdn.net", "sansec.us", "sanguinelab.net", "syteapi.net"]\ncrawls = pyresearch.hostaxis.hostaxis_iterable(hosts, "hosts crawls ", " -c -1")'
               }

def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)

